#!/bin/bash

file=$1
echo $(file $file)
if [[ -x "$file" ]]
then 
	echo "File '$file' is executable"
fi
