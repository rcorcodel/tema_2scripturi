#!/bin/bash

os=$(less /etc/group | grep "devops"| cut -c 1-6)
user=$(awk -F: '{ print $1 }' /etc/passwd | grep "engineer")


#verific daca exista/ adaug grup
if [ "$os" == "devops" ]; then
	echo "DevOps group exists!"
else
	sudo groupadd devops
fi


#verific daca exista /adaug user
if [ "$user" == "engineer" ]; then
	echo "User engineer already exists!"
else
	sudo useradd engineer
fi