#!/bin/bash

path=$1
oldExt=$2
newExt=$3

#verific daca path-ul exista
if [ ! -d "$1" ]; then

	echo "The directory does not exist";

	exit 1
fi
#am facut un loop ca pentru orice fisier cu extensia data din path sa-l redenumeasca in extensia noua
for i in "$1"*$oldExt
do
	if [ -f "$i" ]; then
		mv "$i" "${i%$oldExt}$newExt"
	fi
done