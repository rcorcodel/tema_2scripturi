#!/bin/bash


#din outputul comenzii /etc/os-release am taiat string-ul ID_LIKE ca sa afiseze exact


distribution=$(grep '^ID_LIKE' /etc/os-release | cut -c 9-14)


#instalez git si vim in functie de outputul variabilei care face comparatie intre cele doua string-uri

if [ "$distribution" = "debian" ]; then
	echo "This is Debian"
	sudo apt-get update
	sudo apt-get upgrade 
	sudo apt-get install git
	sudo apt-get install vim 
else
	echo "This is Centos"
	sudo yum check-update
	sudo yum update
	sudo yum install git
	sudo yum install vim
fi


